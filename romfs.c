#define _FILE_OFFSET_BITS  64
#define FUSE_USE_VERSION 26

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <fuse/fuse.h>
#include <error.h>
#include <dirent.h>
#include <sys/types.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>

#define FAILURE -1
#define SUCCESS 0

char *root;

static int print_error(void)
{
	printf("ROM filesystem:Permission Denied\n");
	return FAILURE;
}

static void get_fullpath(char fpath[PATH_MAX], const char *path)
{
	strcpy(fpath, root);
	strncat(fpath, path, PATH_MAX);
}

static int romfs_mknod(const char *path, mode_t mode, dev_t dev)
{
	return print_error();
}

static int romfs_mkdir(const char *path, mode_t mode)
{
	return print_error();
}

static int romfs_unlink (const char *path)
{
	return print_error();
}

static int romfs_rmdir(const char *path)
{
	return print_error();
}

static int romfs_symlink(const char *path, const char *dest_path)
{
	return print_error();
}

static int romfs_rename(const char *path, const char *new_path)
{
	return print_error();
}

static int romfs_link(const char *path, const char *dest_path)
{
	return print_error();
}

static int romfs_chmod(const char *path, mode_t mode)
{
	return print_error();
}

static int romfs_chown(const char *path, uid_t uid, gid_t gid)
{
	return print_error();
}

static int romfs_utimens(const char *path, const struct timespec tv[2])
{
	return print_error();
}

static int romfs_truncate(const char *path, off_t offset)
{
	return print_error();
}

static int romfs_write(const char *path, const char *new_path, size_t size,
		off_t offset, struct fuse_file_info *fi)
{
	return print_error();
}

static int romfs_open(const char *path, struct fuse_file_info *fi)
{
	int fd;
	char fpath[PATH_MAX];

	get_fullpath(fpath, path);

	fd = open(fpath, fi->flags);
	if (fd < 0) {
		printf("Error in opening the file\n");
		return fd;
	}

	fi->fh = fd;

	return SUCCESS;
}

static int romfs_read(const char *path, char *buf, size_t size,
	       off_t offset, struct fuse_file_info *fi)
{
        return pread(fi->fh, buf, size, offset);
}

static int romfs_opendir(const char *path, struct fuse_file_info *fi)
{
	DIR *dp;
        char fpath[PATH_MAX];

	get_fullpath(fpath, path);
	dp = opendir(fpath);
	if (dp == NULL) {
		printf("Error in opening directory\n");
		return FAILURE;
	}

	fi->fh = (intptr_t) dp;

	return SUCCESS;
}

static int romfs_readdir(const char *path, void *buf, fuse_fill_dir_t filler,
		  off_t offset, struct fuse_file_info *fi)
{
	DIR *dp;
	struct dirent *de;

	dp = (DIR *) (uintptr_t) fi->fh;

	de = readdir(dp);
	if (de == 0) {
		printf("Error in reading directory\n");
		return FAILURE;
	}

	do {
		printf("calling filler with name %s\n", de->d_name);
		if (filler(buf, de->d_name, NULL, 0) != 0) {
			printf(" ERROR reading filler:buffer full");
			return FAILURE;
		}
	} while ((de = readdir(dp)) != NULL);

	return SUCCESS;
}

static int romfs_release(const char *path, struct fuse_file_info *fi)
{
	int retstat;

	retstat = close(fi->fh);
	if (retstat == -1) {
		printf("Error in closing the file\n");
		return retstat;
	}

	return retstat;
}

static int romfs_releasedir(const char *path, struct fuse_file_info *fi)
{
	int retstat;

	retstat = closedir((DIR *) (uintptr_t) fi->fh);
	if (retstat == -1) {
		printf("Close operation failure\n");
		return retstat;
	}
	return retstat;
}

static int romfs_getattr(const char *path, struct stat *statbuf)
{
	int retstat;
	char fpath[PATH_MAX];

	get_fullpath(fpath, path);

	retstat = lstat(fpath, statbuf);

	return retstat;
}

static int romfs_readlink(const char *path, char *link, size_t size)
{
	int retstat;
	char fpath[PATH_MAX];

	get_fullpath(fpath, path);

	retstat = readlink(fpath, link, size - 1);
	if (retstat >= 0) {
		link[retstat] = '\0';
		retstat = 0;
	}

	return retstat;
}

static int romfs_statfs(const char *path, struct statvfs *statv)
{
	char fpath[PATH_MAX];

	get_fullpath(fpath, path);
	return statvfs(fpath, statv);
}

static int romfs_getxattr(const char *path, const char *name,
		   char *value, size_t size)
{
	int retstat = 0;
	char fpath[PATH_MAX];

	get_fullpath(fpath, path);
	retstat = lgetxattr(fpath, name, value, size);
	if (retstat >= 0)
		printf("%s\n", value);

	return retstat;
}

static int romfs_flush(const char *path, struct fuse_file_info *fi)
{
	return SUCCESS;
}

static int romfs_listxattr(const char *path, char *list, size_t size)
{
	int retstat = 0;
	char fpath[PATH_MAX];
	char *ptr;

	get_fullpath(fpath, path);

	retstat = llistxattr(fpath, list, size);
	if (retstat >= 0) {
		printf("returned attributes length %d):\n", retstat);
		if (list != NULL)
			for (ptr = list; ptr < list + retstat;
			     ptr += strlen(ptr)+1)
				printf("\"%s\"\n", ptr);
		else
			printf("(null)\n");
	}
	return retstat;
}


struct fuse_operations romfs_op = {
	.unlink = romfs_unlink,
	.mknod = romfs_mknod,
	.mkdir = romfs_mkdir,
	.rmdir = romfs_rmdir,
	.link = romfs_link,
	.symlink = romfs_symlink,
	.rename = romfs_rename,
	.chmod = romfs_chmod,
	.chown = romfs_chown,
	.truncate = romfs_truncate,
	.utimens = romfs_utimens,
	.write = romfs_write,
	.open = romfs_open,
	.read = romfs_read,
	.opendir = romfs_opendir,
	.readdir = romfs_readdir,
	.release = romfs_release,
	.releasedir = romfs_releasedir,
	.getattr = romfs_getattr,
	.readlink = romfs_readlink,
	.statfs = romfs_statfs,
	.getxattr = romfs_getxattr,
	.listxattr = romfs_listxattr,

};


