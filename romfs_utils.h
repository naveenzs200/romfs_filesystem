#ifndef ROMFS_UTILS_H
#define ROMFS_UTILS_H

#define _FILE_OFFSET_BITS  64
#define FUSE_USE_VERSION 26

extern char *root;

extern struct fuse_operations romfs_op;

#define FAILURE -1
#define SUCCESS 0

#endif
