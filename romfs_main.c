#include <romfs_utils.h>
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <fuse/fuse.h>
#include <error.h>
#include <dirent.h>
#include <sys/types.h>
#include <errno.h>
#include <string.h>

char *root;

int main(int argc, char *argv[])
{
	if ((argc < 3) || (argv[argc-2][0] == '-') || (argv[argc-1][0] == '-'))
		error(1, 0, "Usage: ./romfs <source_dir> <mount_point>\n");

	root = realpath(argv[argc-2], NULL);
	argv[argc-2] = argv[argc-1];
	argv[argc-1] = NULL;
	argc--;

	fuse_main(argc, argv, &romfs_op, NULL);
	return SUCCESS;
}
